package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class ValidEmailTest {
	
	// test email address format
	@Test
	public void testisValidEmailFormatRegular() {
		boolean result = ValidEmail.isValidEmail("test@test.com");
		assertTrue("Invalid case",result);
	}
	
	@Test
	public void testisValidEmailFormatException() {
		boolean result = ValidEmail.isValidEmail("");
		assertFalse("Invalid case",result);
	}
	
	@Test
	public void testisValidEmailFormatBoundaryOut() {
		boolean result = ValidEmail.isValidEmail("test@test.com@");
		assertFalse("Invalid case",result);
	}
	
	@Test
	public void testisValidEmailFormatBoundaryIn() {
		boolean result = ValidEmail.isValidEmail("test@test.com");
		assertTrue("Invalid case",result);
	}

	// test account name format
	@Test
	public void testisValidAccountNameRegular() {
		boolean result = ValidEmail.isValidEmail("test11@test.com");
		assertTrue("Invalid case", result);
	}
	
	@Test
	public void testisValidAccountNameException() {
		boolean result = ValidEmail.isValidEmail("@test.com");
		assertFalse("Invalid case",result);
	}
	
	@Test
	public void testisValidAccountNameBoundaryOut() {
		boolean result = ValidEmail.isValidEmail("TEST@test.com");
		assertFalse("Invalid case",result);
	}
	
	@Test
	public void testisValidAccountNameBoundaryIn() {
		boolean result = ValidEmail.isValidEmail("ttt11@test.com");
		assertTrue("Invalid case",result);
	}
	
	// test domain name format
	@Test
	public void testisValidDomainNameRegular() {
		boolean result = ValidEmail.isValidEmail("test@test.com");
		assertTrue("Invalid case",result);
	}
	
	@Test
	public void testisValidDomainNameException() {
		boolean result = ValidEmail.isValidEmail("test@");
		assertFalse("Invalid case",result);
	}
	
	@Test
	public void testisValidDomainNameBoundaryOut() {
		boolean result = ValidEmail.isValidEmail("test@te!.com");
		assertFalse("Invalid case",result);
	}
	
	@Test
	public void testisValidDomainNameBoundaryIn() {

		boolean result = ValidEmail.isValidEmail("test@test.com");
		assertTrue("Invalid case",result);
	}
	
	// test extension name format
	public void testisValidExtensionNameRegular() {
		boolean result = ValidEmail.isValidEmail("test@test.com");
		assertTrue("Invalid case",result);
	}
	
	@Test
	public void testisValidExtensionNameException() {
		boolean result = ValidEmail.isValidEmail("test@test. ");
		assertFalse("Invalid case",result);
	}
	
	@Test
	public void testisValidExtensionNameBoundaryOut() {
		boolean result = ValidEmail.isValidEmail("test@test.#");
		assertFalse("Invalid case",result);
	}
	
	@Test
	public void testisValidExtensionNameBoundaryIn() {
		boolean result = ValidEmail.isValidEmail("test@tes.com");
		assertTrue("Invalid case",result);
	}
}