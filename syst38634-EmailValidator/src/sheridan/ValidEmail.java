package sheridan;

public class ValidEmail {

	public static boolean isValidEmail(String email) {
		
		if(email != null) {
			return email.matches("^[a-z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.a-za-z]+$");
		}
		
		return false;
	}
}
